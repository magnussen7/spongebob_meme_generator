#!/usr/bin/python3
# coding: utf-8
import argparse
import random
import string
from PIL import Image, ImageFont, ImageDraw
import os

class spongebob_generator():
    def __init__(self, text, destination):
        self.text = self.__mocking_generator__(text)
        self.destination = destination
        self.meme = self.__create_image__()

    def __mocking_generator__(self, text):
        mocking = []
        for value in text:
            if bool(random.getrandbits(1)):
                mocking.append(value.upper())
            else:
                mocking.append(value.lower())

        return ''.join(mocking)

    def __create_image__(self):
        original_image = Image.open(os.path.dirname(os.path.realpath(__file__)) + os.sep + 'mocking_spongebob.jpg')
        mocking = ImageDraw.Draw(original_image)
        font = ImageFont.truetype(os.path.dirname(os.path.realpath(__file__)) + os.sep + 'LiberationSans-Bold.ttf', 22)
        width_img, height_img = original_image.size
        width, height = font.getsize(self.text)
        mocking.text(((width_img - width)/2, (height_img - height)), self.text,(255, 255, 255), font=font)

        if self.destination.endswith(os.sep):
            original_image.save(self.destination + 'mocking_sponge_' + ''.join(random.sample(string.ascii_lowercase, 7)) + '.jpg')
        else:
            original_image.save(self.destination + os.sep + 'mocking_sponge_' + ''.join(random.sample(string.ascii_lowercase, 7)) + '.jpg')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Mocking spongebob generator')
    parser.add_argument('text', type=str, help='Text')
    parser.add_argument('destination', nargs='?', type=str, default=os.getcwd(), help='Destination folder')
    args = parser.parse_args()

    spongebob_generator(args.text, args.destination)
