# Mocking SpongeBob Generator

A dumb Python3 script to generate mocking SpongeBob meme.

## Requirements:

- Python3.6
- Pillow==6.2.1

## Installation: 

    git clone https://gitlab.com/magnussen7/spongebob_meme_generator.git
    
    cd spongebob_meme_generator
    
    pip install -r requirements.txt
    
    chmod +x spongebob.py
    
## Usage:

    ./spongebob.py text [destination]

Example:

    ./spongebob.py "Look at my script!"